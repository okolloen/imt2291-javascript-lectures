<?php

$contentType = 'application/json';
if (isset($_GET['contentType']))
	$contentType = $_GET['contentType'];

$charSet = 'utf-8';
if (isset($_GET['charset']))
	$charset = $_GET['charset'];

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
header("Access-Control-Allow-Headers: Origin");
header("Content-Type: $contentType; charset=$charset");

readfile($_GET['url']);