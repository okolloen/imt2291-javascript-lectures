<?php

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
header("Access-Control-Allow-Headers: Origin");
header("Content-Type: application/json; charset=utf-8");

$subjects = array(array('code'=> 'IMT2291', 'name'=> 'WWW-Teknologi'),
		array('code'=> 'IMT1401', 'name'=> 'Informasjons- og publiseringsteknologi'), 
		array('code'=> 'IMT3281', 'name'=> 'Applikasjonsutvikling'));

$arr = array ('name'=> 'Oivind Kolloen', 'subjects'=> $subjects);
echo json_encode ($arr);
